<%-- 
    Document   : mes_annonces
    Created on : 4 mai 2016, 14:56:25
    Author     : rdelas
--%>

<%@include file="includes/header.jsp" %>
<!DOCTYPE html>
<html>
    <%@include file="includes/head.jsp" %>
    <body>
        <%@include file="includes/body_header.jsp" %>
        <%@include file="includes/menu.jsp" %>

        <div>
            <c:forEach var="a" items="${annonces}" varStatus="status">
                <article id="annonce-${status.index}" class="annonce">
                    <div id="affichePhoto">
                        <c:choose>
                            <c:when test="${a.photoUrl == null}" >
                                <img src="images/Photo_not_available.jpg"/>
                            </c:when>
                            <c:otherwise>
                                <img src="images/${a.id}/${a.photoUrl}"/>
                            </c:otherwise>  
                        </c:choose>
                    </div>

                    <div id="afficheMail">
                        <h2>
                            <fmt:formatNumber type="currency" currencySymbol="&euro;" minFractionDigits="2" maxFractionDigits="2" value="${a.prix}"/>
                        </h2>

                        <c:url value="/ServletMail" var="url">
                            <c:param name="id" value="${a.encryptedId}" />
                        </c:url>


                    </div>

                    <div id="afficheAnnonce">
                        <h2 id="titreAnnonce">${a.titre}</h2>

                        <p id="date">
                            <em>
                                <fmt:formatDate type="both" dateStyle="medium" timeStyle="short" value="${a.dateDepot}" />
                            </em>
                        </p>

                        <p id="campusAnnonce"><em>${a.utilisateur.campus.nom}</em></p>

                        <p>
                            <em>
                                ${fn:toUpperCase(fn:substring(a.categorie, 0, 1))}${fn:toLowerCase(fn:substring(a.categorie, 1,-1))}
                            </em>
                        </p>
                        <div class="btns-modify" >
                            <div>
                                <a href="#oModal" class="bouton" onclick="loadModal('${a.encryptedId}')">Modifier</a>
                            </div>
                            <div>
                                <form method="POST" action="ServletAnnonceForm" >
                                    <input type="hidden" name="action" value="delete" />
                                    <input type="hidden" name="id" value="${a.encryptedId}" />
                                    <input type="submit" value="Supprimer" class="bouton"/>
                                </form>
                            </div>
                        </div>
                    </div>

                </article>

            </c:forEach>

        </div>
        <div id='oModal' class='cModalAjoutAnnonce' >
            <div >
                <a href="#" class="close">X</a>
                <div id="innerModal">
                </div>
            </div>
        </div>
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
