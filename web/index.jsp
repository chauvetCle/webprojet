<%-- 
    Document   : index
    Created on : 17 mars 2016, 11:24:53
    Author     : Clem
--%>
<%@include file="includes/header.jsp" %>

<!DOCTYPE html>
<html>
    <jsp:include page="includes/head.jsp" />
    <body onload="getAnnonceList(1);">

        <jsp:include page="includes/body_header.jsp" />

        <c:if test="${user != null}" >            
            <jsp:include page="includes/menu.jsp"/>  
        </c:if>

        <jsp:include page="includes/annonces_display.jsp" />

        <c:if test="${user == null}" >            
            <div id='oModal' class='cModalEnregistrementUser'>
                <div>
                    <a href="#" class="close" id="boutonFermer">X</a>
                    <jsp:include page="/ServletUserForm?action=save"/>
                </div>
            </div>
        </c:if>
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
