<%-- 
    Document   : form_add_produit
    Created on : 20 mars 2016, 14:59:40
    Author     : Clem
--%>

<%@include file="header.jsp" %>
<div>
    <fieldset class="fieldset-form">
        <legend>${titre}</legend>
        <form action="ServletAnnonceForm" method="post" enctype="multipart/form-data">
            <label>Type d'annonce : </label>
            <c:forEach items="${types}" varStatus="stat" var="type" >
                <input class="ratio" id="${"type".concat(stat.index)}" type="radio" name="type" value="${type}" 
                       <c:if test="${stat.first}" >
                           required
                       </c:if>     
                       <c:if test="${annonce.type eq type}" >
                           checked
                       </c:if>
                       />
                <label class="labelAddAnnonce" for="${"type".concat(stat.index)}" >${fn:toUpperCase(fn:substring(type, 0, 1))}${fn:toLowerCase(fn:substring(type, 1,-1))}</label>
            </c:forEach>
            <br/>
            <label for="titre">Titre : </label><br/><input type="text" id="titre" name="titre" value="${annonce.titre}" required/><br/>
            <fmt:formatNumber var="prixStr" value="${annonce.prix}" minFractionDigits="2" maxFractionDigits="2" />
            <fmt:parseNumber var="prix" value="${prixStr}" />
            <label for="prix">Prix : </label><br/><input type="number" id="prix" name="prix" step="0.01" min="0" pattern="\d+((\.|,)\d{2})?" value="${prix}" required/><br/>
            <label for="categorie"> Cat&eacute;gorie : </label><br/>
            <select id="categorie" name="categorie" size="1">
                <c:forEach items="${categories}" var="categorie" >
                    <option value="${categorie}" 
                            <c:if test="${annonce.categorie eq categorie}" >
                                selected
                            </c:if>
                            >
                        ${fn:toUpperCase(fn:substring(categorie, 0, 1))}${fn:toLowerCase(fn:substring(categorie, 1,-1))}
                    </option>
                </c:forEach>
            </select>
            <br/>
            <label for="description">Description : </label><br/>
            <textarea id="description" name="description" required>${annonce.description}</textarea><br>
            <label for="phptoUrl">Photo : </label><br/><input type="file" id="photoUrl" name="photoUrl" value="${annonce.photoUrl}"/><br/>
            <input type="hidden" name="encryptedId" value="${annonce.encryptedId}" />
            <input type="hidden" name="action" value="${action}"/>
            <input type="submit" value="${btnLabel}" name="submit" class="bouton"/>
        </form>
    </fieldset>
    <c:if test="${errors != null}">
        <ul>
            <c:forEach items="${errors}" varStatus="status" var="error" >
                <li>${status.index} : ${error}</li>
                </c:forEach>    
        </ul>
    </c:if>
</div>
