<%-- 
    Document   : menu
    Created on : 4 mai 2016, 11:40:15
    Author     : Delas
--%>


<nav id="menu">
    <ul>
        <li class="active"><a href="index.jsp">Accueil</a></li>
        <li><a href="creer_annonce.jsp">Ajouter une annonce</a></li>
        <li><a href="ServletMesAnnonces">Mes annonces</a></li>
        <li><a href="mon_compte.jsp">Compte</a></li>
    </ul>
</nav>
