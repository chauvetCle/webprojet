<%-- 
    Document   : form_seach_annonce
    Created on : 28 avr. 2016, 13:22:55
    Author     : rdelas
--%>
<%@include file="header.jsp" %>

<form id="annonceSearchForm" method="POST" onsubmit="return getAnnonceList(1, this);">
    <label class="labelRecherche" for="titre"></label><input type="text" id="titre" name="titre" placeholder="Titre"/>
    
    <label class="labelRecherche" for="campusId"></label>
    <select id ="campusId" name="campusId">
        <option disabled selected hidden value="" >Votre campus</option>
        <option value="" >Tous</option>
        <c:forEach items="${campusList}" var="campus">
            <option value="${campus.id}">${campus.nom}</option>
        </c:forEach>
        </option>            
    </select>
    
    <label class="labelRecherche" for="categorie"></label>
    <select id="categorie" name="categorie" size="1">
        <option disabled selected hidden value="" >Catégorie</option>
        <option value="" >Toutes</option>
        <c:forEach items="${categories}" var="categorie" >
            <option value="${categorie}" >${fn:toUpperCase(fn:substring(categorie, 0, 1))}${fn:toLowerCase(fn:substring(categorie, 1,-1))}</option>
        </c:forEach>
    </select>
    <br/>
    <label class="labelRecherche" for="prixMin"></label>
    <input type="number" id="prixMin" name="prixMin" step="0.01" min="0" pattern="\d+((\.|,)\d{2})?" placeholder="Prix minimum"/>

    <label class="labelRecherche" for="prixMin"></label>
    <input type="number" id="prixMax" name="prixMax" step="0.01" min="0" pattern="\d+((\.|,)\d{2})?" placeholder="Prix maximum"/><br/>

    <label class="labelRecherche">Type d'annonce : </label>
    <c:forEach items="${types}" varStatus="stat" var="type" >
        <input class="ratio" id="${"type".concat(stat.index)}" type="radio" name="type" value="${type}" 
               <c:if test="${stat.index eq 1}">checked</c:if>
                   />
               <label for="${"type".concat(stat.index)}" >${fn:toUpperCase(fn:substring(type, 0, 1))}${fn:toLowerCase(fn:substring(type, 1,-1))}</label>
    </c:forEach>
    
    <br/>
    <label class="labelRecherche" for="photo">Photo</label>
    <input class="ratio" type="checkbox" id="photo" name="photo">
        <br/>
        <input type="submit" value="Rechercher" class="bouton"/>
</form>

