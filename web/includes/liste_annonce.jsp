<%-- 
    Document   : liste_annonce
    Created on : 28 mars 2016, 18:41:27
    Author     : Clem
--%>

<%@include file="header.jsp" %>

<c:forEach var="a" items="${listeDesAnnonces}" varStatus="status">
    <div id="annonce-${status.index}" class="annonce">
        <div id="affichePhoto">
            <c:choose>
                <c:when test="${a.photoUrl == null}" >
                    <img src="images/Photo_not_available.jpg"/>
                </c:when>
                <c:otherwise>
                    <img src="images/${a.id}/${a.photoUrl}"/>
                </c:otherwise>  
            </c:choose>
        </div>

        <div id="afficheMail">
            <h2>
                <fmt:formatNumber type="currency" currencySymbol="&euro;" minFractionDigits="2" maxFractionDigits="2" value="${a.prix}"/>
            </h2>

            <c:url value="/ServletMail" var="url">
                <c:param name="id" value="${a.encryptedId}" />
            </c:url>

            <a href="${url}" class="bouton" >Me contacter</a>
        </div>

        <div id="afficheAnnonce">
            <h2 id="titreAnnonce">${a.titre}</h2>

            <p id="date">
                <em>
                    <fmt:formatDate type="both" dateStyle="medium" timeStyle="short" value="${a.dateDepot}" />
                </em>
            </p>

            <p id="campusAnnonce"><em>${a.utilisateur.campus.nom}</em></p>

            <p>
                <em>
                    ${fn:toUpperCase(fn:substring(a.categorie, 0, 1))}${fn:toLowerCase(fn:substring(a.categorie, 1,-1))}
                </em>
            </p>

            <p>${a.description}</p>

        </div>

    </div>

</c:forEach>


<c:if test="${(nbPages-1) gt 1}" >
    <div class="pagination">
        <a href="#" onclick="return getAnnonceList(1);">&lt;&lt;</a>
        <a href="#" onclick="return getAnnonceList(${(currentPage-1==1)?1:currentPage-1});">&lt;</a>
        <c:set var="begin" value="${(currentPage-2) le 0 ? 1 : ((currentPage+2) ge nbPages) ? nbPages-5 : (currentPage-2) }" />
        <c:set var="end" value="${((currentPage+2) ge nbPages) ? nbPages : (currentPage-2) le 0 ? 5 : (currentPage+2) }" />
        <c:forEach begin="${begin}" 
                   end="${end}"
                   varStatus="status">
            <a href="#" onclick="return getAnnonceList(${status.index});"
               <c:if test="${status.index eq currentPage}" >
                   style="font-weight: bold;"
               </c:if>
               >${status.index}</a>
        </c:forEach>
        <a href="#" onclick="return getAnnonceList(${(currentPage+1==nbPages)?nbPages:currentPage+1});">&gt;</a>
        <a href="#" onclick="return getAnnonceList(${nbPages});">&gt;&gt;</a>
    </div>
</c:if>
