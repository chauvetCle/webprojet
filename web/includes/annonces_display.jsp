<%-- 
    Document   : annonces_display
    Created on : 4 mai 2016, 19:58:30
    Author     : rdelas
--%>

<%@include file="header.jsp" %>
<jsp:include page="/ServletAnnonceSearch"/>

<div id="list_results">
    <jsp:include page="/ServletAnnoncePaginate" />
</div>
