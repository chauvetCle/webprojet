/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.validation.ConstraintViolation;
import model.entity.bean.Annonce;
import model.entity.bean.CateAnnonce;
import model.entity.bean.TypeAnnonce;
import model.entity.bean.Utilisateur;
import model.entity.services.AnnonceServices;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import view.servlet.form.AnnonceFormBean;
import view.servlet.form.Bean;

/**
 *
 * @author Clem
 */
@WebServlet(name = "ServletAnnonceForm", urlPatterns = {"/ServletAnnonceForm"})
@MultipartConfig
public class ServletAnnonceForm extends HttpServlet {

    @EJB
    private AnnonceServices annonceServices;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession() == null) {
            response.sendRedirect("index.jsp");
            return;
        } else if (request.getSession().getAttribute("user") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        addData(request);
        RequestDispatcher dp = request.getRequestDispatcher("includes/form_add_annonce.jsp");
        dp.include(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession() == null) {
            response.sendRedirect("index.jsp");
            return;
        } else if (request.getSession().getAttribute("user") == null) {
            response.sendRedirect("index.jsp");
            return;
        }

        String action = request.getParameter("action");
        if (StringUtils.isNotBlank(action) && action.equals("delete")) {
            String id = request.getParameter("id");

            if (StringUtils.isNotBlank(id)) {
                annonceServices.delete(id);
            }
        } else {
            AnnonceFormBean form = AnnonceFormBean.createFromRequestParameters(request.getParameterMap());

            if (!form.validate()) {
                Map<String, String> errors = new HashMap<>();
                Set<ConstraintViolation<Bean>> validationErrors = form.getErrors();
                validationErrors.stream().forEach((error) -> {
                    errors.put(error.getPropertyPath().toString(), error.getMessage());
                });
                request.setAttribute("errors", errors);
            } else {
                Utilisateur u = (Utilisateur) request.getSession().getAttribute("user");
                Annonce a = null;
                final Part filePart = request.getPart("photoUrl");
                final String fileName = getFileName(filePart, "photoUrl");                
                form.setPhotoUrl(fileName);
                if (form.getAction().equals("save")) {
                    Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("user");
                    a = annonceServices.creerAnnonce(form, utilisateur);
                } else {
                    a = annonceServices.updateAnnonce(form, u);
                }

                if (a != null) {
                    uploadImage(filePart, fileName, a);
                }
            }
        }
        response.sendRedirect("ServletMesAnnonces");

    }

    private void uploadImage(final Part filePart, final String fileName, final Annonce a) throws IOException, ServletException {
  
        ServletContext ctx = getServletContext();
        String path = ctx.getRealPath("/images");

        String filePath = path + File.separator + a.getId() + File.separator + fileName;
        System.out.println(filePath);
        File file = new File(filePath);
        FileUtils.forceMkdirParent(file);
        file.createNewFile();

        try (FileOutputStream out = new FileOutputStream(file);
                InputStream filecontent = filePart.getInputStream()) {

            final byte[] bytes = IOUtils.toByteArray(filecontent);
            out.write(bytes);

            System.out.println(String.format("File {0} being uploaded to {1}",
                    new Object[]{fileName, path}));
        } catch (FileNotFoundException fne) {

            System.err.println(String.format("Problems during file upload. Error: {0}",
                    new Object[]{fne.getMessage()}));
        }
    }

    private String getFileName(final Part part, final String partName) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println(partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    private void addData(HttpServletRequest request) {

        String action = request.getParameter("action");
        String titre = "Ajouter une annonce", btnLabel = "Ajouter";
        if (StringUtils.isNotBlank(action) && action.equals("modify")) {
            titre = "Modifier une annonce";
            btnLabel = "Modifier";

            String id = request.getParameter("id");
            Annonce annonce = annonceServices.getAnnonceEncryptedIds(id);
            request.setAttribute("annonce", annonce);
        }

        request.setAttribute("action", action);
        request.setAttribute("titre", titre);
        request.setAttribute("btnLabel", btnLabel);
        request.setAttribute("types", TypeAnnonce.values());
        request.setAttribute("categories", CateAnnonce.values());
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
